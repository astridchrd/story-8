from django.test import TestCase, Client, LiveServerTestCase
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class BookTest(TestCase):
    def test_ada_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'buku.html')

    def test_ada_searchbar(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn ("<button", content) 

    def test_ada_tabel(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn ("<table", content) 


class BookFunctionalTest(LiveServerTestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(BookFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(BookFunctionalTest, self).tearDown()

    def test_if_search_clicked_show_content(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        searchbox = selenium.find_element_by_id("search")
        searchbox.send_keys("Matematika")

        time.sleep(5)
        
        btnSearch = selenium.find_element_by_id("button")
        btnSearch.click()

        time.sleep(5)
        #self.assertIn('30 Menit Kuasai Semua Rumus Matematika SMP', selenium.page_source)
        #time.sleep(2)
